'use strict'

document.addEventListener('DOMContentLoaded', function () {
  const tabItems = document.querySelectorAll('.our-services-block-text');
  const descriptorItems = document.querySelectorAll('.our-services-descriptor-item');

  tabItems.forEach((tab, index) => {
    tab.addEventListener('click', function () {
      tabItems.forEach(tab => tab.classList.remove('active-navbar'));
      descriptorItems.forEach(item => item.classList.remove('active'));
      tab.classList.add('active-navbar');
      descriptorItems[index].classList.add('active');
    });
  });
});


document.addEventListener("DOMContentLoaded", function () {
  const loadMoreButton = document.getElementById("loadMoreButton");
  const imageGrid = document.querySelector(".our-amazing-all");
  const filterButtons = document.querySelectorAll(".our-text");

  const imagesData = [
    { category: "graphic-design", src: "./img/additional-photos1.png" },
    { category: "web-design", src: "./img/additional-photos2.png" },
    { category: "wordpress", src: "./img/additional-photos3.png" },
    { category: "landing-pages", src: "./img/additional-photos4.png" },
    { category: "graphic-design", src: "./img/additional-photos5.png" },
    { category: "web-design", src: "./img/additional-photos6.png" },
    { category: "wordpress", src: "./img/additional-photos7.png" },
    { category: "landing-pages", src: "./img/additional-photos8.png" },
    { category: "graphic-design", src: "./img/additional-photos9.png" },
    { category: "web-design", src: "./img/additional-photos10.png" },
    { category: "wordpress", src: "./img/additional-photos11.png" },
    { category: "landing-pages", src: "./img/additional-photos12.png" },
  ];

  function createImageBlock(data) {
    const li = document.createElement("li");
    li.className = "our-amazing-block";
    li.dataset.filter = data.category;

    const img = document.createElement("img");
    img.className = "our-amazing-block-img";
    img.src = data.src;
    img.width = 285;
    img.height = 211;
    img.alt = "";

    const divInfoWork = document.createElement("div");
    divInfoWork.className = "info-work";
    divInfoWork.innerHTML = `<div class="info-work">
      <div class="info-work-link-svg">
          <a class="link-svg" href="#">
              <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd" clip-rule="evenodd" d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z" fill="#1FDAB5"></path>
              </svg>
          </a>
          <a class="link-svg-2" href="#">
              <svg width="12" height="11" viewBox="0 0 12 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <rect width="12" height="11" fill="white"></rect>
              </svg>
          </a>
      </div>
      <h4 class="text-creative-design">creative design</h4>
      <p class="text-web-design">Web Design</p>
  </div>` ;

    li.appendChild(img);
    li.appendChild(divInfoWork);
    imageGrid.appendChild(li);
  }

  function filterImages(category) {
    const imageBlocks = document.querySelectorAll(".our-amazing-block");
    imageBlocks.forEach((block) => {
      if (category === "all" || block.dataset.filter === category) {
        block.style.display = "block";
      } else {
        block.style.display = "none";
      }
    });
  }


  loadMoreButton.addEventListener("click", function () {
    loadMoreButton.innerHTML = "LOADING...";

    setTimeout(function () {
      for (let i = 0; i < 12; i++) {
        const randomIndex = Math.floor(Math.random() * imagesData.length);
        const randomImageData = imagesData[randomIndex];
        createImageBlock(randomImageData);
      }

      loadMoreButton.style.display = "none";
    }, 1500);
  });

  filterButtons.forEach((button) => {
    button.addEventListener("click", function () {
      const category = button.dataset.workitem;
      filterImages(category);
    });
  });
});





// Карусель от которой голова как карусель//

let btnPrevious = document.querySelector('.btn-previous');
let btnNext = document.querySelector('.btn-next');
let listPicTeam = [...document.querySelectorAll('.team-navbar-item .img-icon')];
let currentIndex = listPicTeam.findIndex(e => e.parentNode.classList.contains('team-navbar-item-active'));
let listTeamInfo = [...document.querySelectorAll('.team-list .team-item')];

btnPrevious.addEventListener('click', showPreviSlide);
btnNext.addEventListener('click', showNextSlide);

listPicTeam.forEach((pic, index) => {
    pic.addEventListener('click', () => {
        if (index !== currentIndex) {
            listTeamInfo.forEach(el => el.classList.remove('team-item-active', 'remove-slide-info'));
            listTeamInfo[index].classList.add('team-item-active', 'active-slide-info');
            listTeamInfo[currentIndex].classList.add('remove-slide-info');
            currentIndex = index;
        }
    });
});

function showNextSlide() {
    listTeamInfo[currentIndex].classList.add('remove-slide-info');
    setTimeout(() => {
        currentIndex = (currentIndex + 1) % listPicTeam.length;
        showSlide(currentIndex);
    }, 1000);
}

function showPreviSlide() {
    listTeamInfo[currentIndex].classList.add('remove-slide-info');
    setTimeout(() => {
        currentIndex = (currentIndex - 1 + listPicTeam.length) % listPicTeam.length;
        showSlide(currentIndex);
    }, 1000);
}

function showSlide(index) {
    listPicTeam.forEach((elem, i) => {
        if (i === index) {
            elem.parentNode.classList.add('team-navbar-item-active');
            listTeamInfo.forEach(el => (el.dataset.info === elem.dataset.slid) ? el.classList.add('team-item-active', 'active-slide-info') : el.classList.remove('active-slide-info'));
        } else {
            elem.parentNode.classList.remove('team-navbar-item-active');
        }
    });
}
